= Emerging Fedora Desktops
:page-layout: without_menu

xref:fedora-silverblue::index.adoc[Fedora Silverblue]
Fedora Silverblue is an immutable desktop operating system featuring the _GNOME_ desktop. It aims to be extremely stable and reliable. It also aims to be an excellent platform for developers and for those using container-focused workflows.

xref:fedora-kinoite::index.adoc[Fedora Kinoite]
Fedora Kinoite is an immutable desktop operating system featuring the _KDE_ desktop It aims to be extremely stable and reliable. It also aims to be an excellent platform for developers and for those using container-focused workflows.
