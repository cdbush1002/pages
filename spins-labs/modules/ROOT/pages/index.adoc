= Fedora Spins & Labs
:page-layout: without_menu


== Fedora Spins

While Fedora Workstation, the main desktop edition, provides the GNOME desktop as the user GUI, many Fedorians prefer one of the many functional alternatives.   

To provide this in the same high-quality and reliable way, community members have developed the Spins - autonomously installable desktop system alternatives with the otherwise same functionality. The spectrum ranges from the heavyweight alternative KDE to resource-saving alternatives like Xfce.

You can download the appropriate spin and use it to install Fedora, pre-configured with the working environment of your choice, at https://spins.fedoraproject.org/ 



== Fedora Labs

Users adopt Fedora for a wide variety of purposes and in a wide variety of domains. For several of these, community members have put together _functional bundles_ that optimally support these works. This is how a distinctive collection was created - the Fedora Labs.

They range from Jam,dedicated to audio and music enthusiasts, to Security Lab, dedicated to security auditing, forensics and penetration testing, to Scientific, a collection of the most useful open source scientific and numerical tools – just to name a few of the currently 9 available bundles.

The respective compilations are either installed as independent, complete Fedora versions or complement existing Fedora installations. They are maintained, serviced and independently documented by members of the Fedora community.

You find the complete collection at https://labs.fedoraproject.org/




//https://fedoraproject.org/wiki/Fedora_jam[Jam]::
//A spin dedicated to audio and music enthusiasts. 

//https://fedoraproject.org/wiki/Security_Lab[Security Lab]::
//The Fedora Security Spin is a live media based on Fedora to provide a safe test environment for working //on security auditing, forensics and penetration testing, coupled with all the Fedora Security features //and tools.

//https://fedoraproject.org/wiki/Robotics_Spin[Robotics Suite]::
//Create a spin that provides an out-of-the-box usable robotic simulation environment featuring a linear //demo to introduce new users. Additionally we want to add as many robotics related packages to the spin //to maximize out-of-the-box usable hardware and software. 

//https://fedora-scientific.readthedocs.io/en/latest/[Scientific]::
//Wary of reinstalling all the essential tools for your scientific and numerical work? The answer is //here. Fedora Scientific Spin brings together the most useful open source scientific and numerical tools //atop the goodness of the KDE desktop environment.


//__**To be completed**__
